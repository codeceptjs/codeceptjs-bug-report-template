Feature("Bug Report Template");

Scenario(
    "WebDriver grabBrowserLogs",
    async I => {
        I.amOnPage("https://nextjs.pbeigang.now.sh");
        const logs = await I.grabBrowserLogs();
        console.log(JSON.stringify(logs))
    }
);
