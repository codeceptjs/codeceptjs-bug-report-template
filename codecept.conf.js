let config = {
  tests: "./*_test.js",
  output: "./output",
  helpers: {
    WebDriver: {
      url: "http://the-internet.herokuapp.com",
      browser: "chrome",
      windowSize: "1920x1080"
    }
  },
  include: {
    I: "./steps_file.js"
  },
  bootstrap: null,
  mocha: {},
  name: "codeceptjs-bug-report-template"
};

if (process.profile === "chrome-ci") {
  config.helpers.WebDriver.host =
    process.env.SELENIUM_STANDALONE_CHROME_PORT_4444_TCP_ADDR;
  config.helpers.WebDriver.protocol = "http";
  config.helpers.WebDriver.port = 4444;
}

exports.config = config;
